//
//  ViewController.m
//  EvernoteDemo
//
//  Created by Peter Pan on 1/11/14.
//  Copyright (c) 2014 Peter Pan. All rights reserved.
//

#import "ViewController.h"
#import "EvernoteSDK.h"
#import "NSData+EvernoteSDK.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *evernoteLabel;


@end

@implementation ViewController

- (IBAction)loginButPressed:(id)sender {

    
    EvernoteSession *session = [EvernoteSession sharedSession];

    
    [session authenticateWithViewController:self completionHandler:^(NSError *error) {
        if (error || !session.isAuthenticated){
            if (error) {
                NSLog(@"Error authenticating with Evernote Cloud API: %@", error);
            }
            if (!session.isAuthenticated) {
                NSLog(@"Session not authenticated");
            }
        } else {
            // We're authenticated!
            EvernoteUserStore *userStore = [EvernoteUserStore userStore];
            
            [userStore getUserWithSuccess:^(EDAMUser *user) {
                // success
                NSLog(@"Authenticated as %@", [user username]);
            } failure:^(NSError *error) {
                // failure
                NSLog(@"Error getting user: %@", error);
            } ];
        }
    }];
}

- (IBAction)shareButPressed:(id)sender {
    
    EvernoteSession *session = [EvernoteSession sharedSession];
    if(session.isAuthenticated)
    {
        NSString* filePath = [[NSBundle mainBundle] pathForResource:@"wealthy" ofType:@"png"];
        NSData *myFileData = [NSData dataWithContentsOfFile:filePath];
        NSData *dataHash = [myFileData enmd5];
        EDAMData *edamData = [[EDAMData alloc] initWithBodyHash:dataHash size:myFileData.length body:myFileData];
        EDAMResource* resource = [[EDAMResource alloc] initWithGuid:nil noteGuid:nil data:edamData mime:@"image/png" width:0 height:0 duration:0 active:0 recognition:0 attributes:nil updateSequenceNum:0 alternateData:nil];
        
        ENMLWriter* myWriter = [[ENMLWriter alloc] init];
        [myWriter startDocument];
        [myWriter writeRawString:@"Wealthy! - Take photo, share, and track expenses at one step!"];
        [myWriter startElement:@"span"];
        [myWriter startElement:@"br"];
        [myWriter endElement];
        [myWriter writeResource:resource];
        [myWriter endElement];
        [myWriter endDocument];
        NSString *noteContent = myWriter.contents;
        
        NSMutableArray* resources = [NSMutableArray arrayWithArray:@[resource]];
        
        
        EDAMNote *newNote = [[EDAMNote alloc] init];
        [newNote setTitle:@"Wealthy!"];
        [newNote setContent:noteContent];
        [newNote setContentLength:noteContent.length];
        
        
        [newNote setResources:resources];
        
        [[EvernoteNoteStore noteStore] createNote:newNote success:^(EDAMNote *note) {
            //   [self.activityIndicatorView stopAnimating];
            NSLog(@"Note created successfully.");
        } failure:^(NSError *error) {
            NSLog(@"Error creating note : %@",error);
            // [self.activityIndicatorView stopAnimating];
        }];

    }
    
   
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    EvernoteSession *session = [EvernoteSession sharedSession];
    
    
    if(session.isAuthenticated)
    {

        EvernoteUserStore *userStore = [EvernoteUserStore userStore];
        

        [userStore getUserWithSuccess:^(EDAMUser *user) {

            // success
            self.evernoteLabel.text = [NSString stringWithFormat:@"evernote:%@", [user username]];
            
        } failure:^(NSError *error) {
            // failure
        } ];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
